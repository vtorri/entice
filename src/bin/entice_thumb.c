/* Entice - small and simple image viewer using the EFL
 * Copyright (C) 2023 Vincent Torri
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <config.h>

#include <Elementary.h>

/*============================================================================*
 *                                  Local                                     *
 *============================================================================*/

static void
_entice_thumb_sha1_str(unsigned char sha[20], char shastr[41])
{
   const char *chmap = "0123456789abcdef";
   int i;

   for (i = 0; i < 20; i++)
     {
        shastr[(i * 2)    ] = chmap[(sha[i] >> 4) & 0xf];
        shastr[(i * 2) + 1] = chmap[ sha[i]       & 0xf];
     }
   shastr[i * 2] = 0;
}

/*============================================================================*
 *                                 Global                                     *
 *============================================================================*/

Eina_Bool
entice_thumb_sha1(const char *file, unsigned char dst[20])
{
    char buf[128];

#ifdef _WIN32
    BY_HANDLE_FILE_INFORMATION info;
    HANDLE h;
    LARGE_INTEGER size;
    LARGE_INTEGER file_index;
    LARGE_INTEGER creation;
    LARGE_INTEGER last_write;
    BOOL res;

    h = CreateFile(file, GENERIC_READ, FILE_SHARE_READ,
                   NULL, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,
                   NULL);

    if (h == INVALID_HANDLE_VALUE)
        return EINA_FALSE;

    res = GetFileInformationByHandle(h, &info);
    CloseHandle(h);
    if (!res)
        return EINA_FALSE;

    size.LowPart = info.nFileSizeLow;
    size.HighPart = info.nFileSizeHigh;
    file_index.LowPart = info.nFileIndexLow;
    file_index.HighPart = info.nFileIndexHigh;
    creation.LowPart = info.ftCreationTime.dwLowDateTime;
    creation.HighPart = info.ftCreationTime.dwHighDateTime;
    last_write.LowPart = info.ftLastWriteTime.dwLowDateTime;
    last_write.HighPart = info.ftLastWriteTime.dwHighDateTime;

    snprintf(buf, sizeof(buf),
             "%llu %llu %llu %llu %llu %llu",
             (unsigned long long)(info.dwVolumeSerialNumber),
             (unsigned long long)(info.dwFileAttributes),
             (unsigned long long)(size.QuadPart),
             (unsigned long long)(file_index.QuadPart),
             (unsigned long long)(creation.QuadPart),
             (unsigned long long)(last_write.QuadPart));
#else
# ifdef STAT_NSEC
#  define STAT_NSEC_MTIME(st) (unsigned long long)((st).st_mtimensec)
#  define STAT_NSEC_CTIME(st) (unsigned long long)((st).st_ctimensec)
# else
#  define STAT_NSEC_MTIME(st) (unsigned long long)(0)
#  define STAT_NSEC_CTIME(st) (unsigned long long)(0)
# endif
    struct stat st;

    if (stat(file, &st) != 0)
        return EINA_FALSE;

    snprintf(buf, sizeof(buf),
             "%llu %llu %llu %llu %llu %llu %llu %llu",
             (unsigned long long)(st.st_mode),
             (unsigned long long)(st.st_uid),
             (unsigned long long)(st.st_gid),
             (unsigned long long)(st.st_size),
             (unsigned long long)(st.st_mtime),
             (unsigned long long)(st.st_ctime),
             STAT_NSEC_MTIME(st),
             STAT_NSEC_CTIME(st));
#endif

    eina_sha1((unsigned char *)buf, strlen(buf), dst);

    return EINA_TRUE;
}

Eina_Strbuf *
entice_thumb_find(const char *path)
{
    unsigned char sha[20];
    char sha_str[41];
    Eina_Strbuf *buf;

    buf = eina_strbuf_new();
    if (!buf)
        return NULL;

    eina_sha1((const unsigned char *)path, strlen(path), sha);
    _entice_thumb_sha1_str(sha, sha_str);

    eina_strbuf_append(buf, efreet_cache_home_get());
    eina_strbuf_append_length(buf, "/entice/thumbs/", sizeof("/entice/thumbs/") - 1);
    eina_strbuf_append_char(buf, sha_str[0]);
    eina_strbuf_append_char(buf, sha_str[1]);
    eina_strbuf_append_char(buf, '/');
    eina_strbuf_append_length(buf, sha_str, 40);
    eina_strbuf_append(buf, ".eet");

    return buf;
}
