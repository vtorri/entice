/* Entice - small and simple image viewer using the EFL
 * Copyright (C) 2023 Vincent Torri
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <config.h>

#include <Elementary.h>

#include "entice_private.h"
#include "entice_config.h"
#include "entice_image.h"
#include "entice_thumb.h"
#include "entice_win.h"
#include "entice_winlist.h"

/*============================================================================*
 *                                  Local                                     *
 *============================================================================*/

static Eina_Bool
_entice_winlist_progressbar_value_set(void *data)
{
    Ecore_Timer *timer;
    Evas_Object *pb;
    double progress;
    Entice_List_Data *d = data;

    pb = evas_object_data_get(d->tb, "progressbar");
    progress = elm_progressbar_value_get (pb);
    if (progress < 1.0) progress += 0.0123;
    else progress = 0.0;
    elm_progressbar_value_set(pb, progress);

    if (progress < 1.0) return ECORE_CALLBACK_RENEW;

    timer = evas_object_data_del(d->tb, "timer");
    if (timer)
        ecore_timer_del(timer);

    return ECORE_CALLBACK_CANCEL;
}

static void
_entice_winlist_table_del_cb(void *data EINA_UNUSED, Evas *e EINA_UNUSED, Evas_Object *obj, void *event_info EINA_UNUSED)
{
    Ecore_Timer *timer;

    timer = evas_object_data_del(obj, "timer");
    if (timer)
        ecore_timer_del(timer);
}

static void
entice_winlist_genlist_sel(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   /* printf("sel item data [%p] on genlist obj [%p], item pointer [%p], index [%d]\n", */
   /*        data, obj, event_info, elm_genlist_item_index_get(event_info)); */
}

static const char *
_entice_winlist_mime_get(const char *file)
{
  if (eina_fnmatch("*.edj", file, EINA_FNMATCH_CASEFOLD))
    return "application/x-edje";
  return efreet_mime_type_get(file);
}

static Evas_Object *
entice_winlist_genlist_content_get(void *data, Evas_Object *obj, const char *part)
{
    Entice_List_Data *d = data;
    Eina_Bool ok = EINA_FALSE;

    d->tb = elm_table_add(obj);
    evas_object_show(d->tb);

    evas_object_event_callback_add(d->tb, EVAS_CALLBACK_DEL,
                                   _entice_winlist_table_del_cb, NULL);

    if (!strcmp(part, "elm.swallow.icon"))
    {
        Evas_Object *o;
        Ecore_Timer *timer;
        Eina_Strbuf *buf;

        // TODO: check if d->path needs thumbnail (image, edje, ...)

        buf = entice_thumb_find(d->path);
        if (buf)
        {
            Eet_File *ef;
            const char *thumb;

            thumb = eina_strbuf_string_get(buf);

            ef = eet_open(thumb, EET_FILE_MODE_READ);
            if (ef)
            {
                unsigned char sha[20];

                 /* thumb file exists - check meta data */
                if (entice_thumb_sha1(d->path, sha))
                {
                    char *sha_orig;
                    int size = 0;

                    sha_orig = eet_read(ef, "orig/stat/sha1", &size);
                    if ((sha_orig) && (size == 20))
                    {
                        if (!memcmp(sha, sha_orig, 20))
                            ok = EINA_TRUE; // sha1 of stat data matches - ok
                    }
                }
                eet_close(ef);
                if (!ok)
                {
                    printf("thumb stat data does not math file stat data\n");
                    fflush(stdout);
                }
            }
            else
            {
                Eina_Strbuf *cmd;

                cmd = eina_strbuf_new();
                if (cmd)
                {
                    Ecore_Exe *exe;
                    char *s;

                    eina_strbuf_append(cmd, PACKAGE_BIN_DIR);
                    eina_strbuf_append(cmd, "/entice_thumb_gen");
#if _WIN32
                    eina_strbuf_append(cmd, ".exe");
#endif
                    eina_strbuf_append_char(cmd, ' ');
                    s = ecore_file_escape_name(d->path);
                    eina_strbuf_append(cmd, d->path);
                    free(s);
                    eina_strbuf_append_char(cmd, ' ');
                    eina_strbuf_append(cmd, _entice_winlist_mime_get(d->path));
                    eina_strbuf_append_char(cmd, ' ');
                    s = ecore_file_escape_name(thumb);
                    eina_strbuf_append(cmd, thumb);
                    free(s);
                    exe = ecore_exe_run(eina_strbuf_string_get(cmd), NULL);
                    eina_strbuf_free(cmd);
                    cmd = NULL;
                }
            }
            eina_strbuf_free(buf);
            buf = NULL;
        }

        if (ok)
        {
            /* display thumb */
            o = entice_image_add(obj);
            elm_table_pack(d->tb, o, 0, 0, 1, 1);
            evas_object_show(o);
        }
        else
        {
            /* wait for thumb to be created */
            o = elm_progressbar_add(obj);
            elm_object_style_set(o, "wheel");
            elm_progressbar_pulse_set(o, EINA_TRUE);
            elm_progressbar_pulse(o, EINA_TRUE);
            elm_table_pack(d->tb, o, 0, 0, 1, 1);
            evas_object_show(o);
            evas_object_data_set(d->tb, "progressbar", o);
            {
                int w, h;
                evas_object_geometry_get(o, NULL, NULL, &w, &h);
                printf(" * %d %d\n", w, h);
                fflush(stdout);
            }

            timer = evas_object_data_get(d->tb, "timer");
            if (!timer)
            {
                timer = ecore_timer_add(0.1,
                                        _entice_winlist_progressbar_value_set, data);
                evas_object_data_set(d->tb, "timer", timer);
            }
        }
    }

    return d->tb;
}

static void
_entice_winlist_genlist_del(void *data, Evas_Object *obj EINA_UNUSED)
{
    Entice_List_Data *d = data;
    Evas_Object *pb;
    Ecore_Timer *timer;

    /* in case some progressbar have not been built */
    pb = evas_object_data_get(d->tb, "progressbar");
    if (pb)
        elm_progressbar_pulse_set(pb, EINA_FALSE);

    timer = evas_object_data_del(d->tb, "timer");
    if (timer)
    {
        ecore_timer_del(timer);
        timer = NULL;
    }

}

static void
hints_changed(void *data, Evas *e EINA_UNUSED, Evas_Object *obj, void *event_info EINA_UNUSED)
{
   int w, h;
   /* Evas_Aspect_Control aspect; */

   /* evas_object_size_hint_aspect_get(obj, &aspect, &w, &h); */
   /* evas_object_size_hint_aspect_set(data, aspect, w, h); */
   evas_object_size_hint_min_get(obj, &w, &h);
   printf(" *** hints min %d %d\n", w, h);
   /* evas_object_size_hint_min_set(data, w, h); */

   evas_object_size_hint_max_get(obj, &w, &h);
   printf(" *** hints min %d %d\n", w, h);
   /* evas_object_size_hint_max_set(data, w, h); */
}

/*============================================================================*
 *                                 Global                                     *
 *============================================================================*/

void
entice_winlist_show(Evas_Object *win)
{
    Entice *entice;
    Eina_List *ll;
    Entice_List_Data *data;
    Elm_Scroller_Policy spv;

    entice = evas_object_data_get(win, "entice");

    if (entice->wl_gl)
        return;

    entice->wl_gl =  elm_genlist_add(win);
    /* for perf : homogeneous */
    elm_genlist_homogeneous_set(entice->wl_gl, EINA_TRUE);
    /* remove horizontal policy to get the size of the item */
    elm_scroller_policy_get(entice->wl_gl, NULL, &spv);
    elm_scroller_policy_set(entice->wl_gl, ELM_SCROLLER_POLICY_OFF, spv);
    evas_object_size_hint_align_set(entice->wl_gl, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_size_hint_weight_set(entice->wl_gl, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_event_callback_add(entice->wl_gl, EVAS_CALLBACK_CHANGED_SIZE_HINTS, hints_changed, entice);
    evas_object_show(entice->wl_gl);

    entice->wl_gl_ic = elm_genlist_item_class_new();
    entice->wl_gl_ic->item_style = "one_icon";
    entice->wl_gl_ic->func.content_get = entice_winlist_genlist_content_get;
    entice->wl_gl_ic->func.del = _entice_winlist_genlist_del;

    EINA_LIST_FOREACH(entice->images, ll, data)
    {
        data->item = elm_genlist_item_append(entice->wl_gl,
                                             entice->wl_gl_ic,
                                             data,
                                             NULL,
                                             ELM_GENLIST_ITEM_NONE,
                                             entice_winlist_genlist_sel,
                                             NULL);
    }

    elm_object_part_content_set(entice->layout, "entice.winlist", entice->wl_gl);

   elm_layout_signal_emit(entice->layout, "state,winlist,show", "entice");
}

void
entice_winlist_hide(Evas_Object *win)
{
    Entice *entice;

    entice = evas_object_data_get(win, "entice");

    if (!entice->wl_gl)
        return;

    elm_genlist_item_class_free(entice->wl_gl_ic);
    evas_object_del(entice->wl_gl);

    entice->wl_gl = NULL;

    elm_layout_signal_emit(entice->layout, "state,winlist,hide", "entice");
}

void
entice_winlist_toggle(Evas_Object *win)
{
    Entice *entice;

    entice = evas_object_data_get(win, "entice");

    if (entice->wl_gl)
        entice_winlist_hide(win);
    else
        entice_winlist_show(win);
}
