/* Entice - small and simple image viewer using the EFL
 * Copyright (C) 2024 Vincent Torri
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ENTICE_THUMB_GEN_H
#define ENTICE_THUMB_GEN_H

typedef struct
{
  char *url;
  int   w, h;
} Search_Result;

extern Evas_Object *win;
extern Evas_Object *subwin;
extern Evas_Object *image;


void thumb_search_image(const char *str,
                        void (*cb)(void *data, Eina_List *results_orig,
                                   Eina_List *results_cached),
                        void *data);

int thumb_edje(Eet_File *ef,
               const char *path,
               const char *mime EINA_UNUSED,
               const char *thumb EINA_UNUSED);

int thumb_font(Eet_File *ef,
               const char *path,
               const char *mime EINA_UNUSED,
               const char *thumb EINA_UNUSED);

int thumb_image(Eet_File *ef,
                const char *path,
                const char *mime EINA_UNUSED,
                const char *thumb EINA_UNUSED);

int thumb_music(Eet_File *ef,
                const char *path,
                const char *mime EINA_UNUSED,
                const char *thumb EINA_UNUSED);

int thumb_paged(Eet_File *ef,
                const char *path,
                const char *mime EINA_UNUSED,
                const char *thumb EINA_UNUSED);

int thumb_video(Eet_File *ef,
                const char *path,
                const char *mime EINA_UNUSED,
                const char *thumb EINA_UNUSED);

void thumb_image_write(Eet_File *ef,
                       const char *key,
                       Evas_Object *img,
                       Eina_Bool a,
                       Eina_Bool lossy);

void thumb_url_str_get(const char *url,
                       size_t max,
                       void (*cb)(void *data, const char *result),
                       const void *data);

void thumb_url_bin_get(const char *url,
                       size_t max,
                       void (*cb)(void *data, const void *result, size_t size),
                       const void *data);

void util_file_mode_parent_copy(const char *file, Eina_Bool is_dir);

static inline Eina_Bool
_entice_thumb_check_image(const char *mime)
{
  return eina_fnmatch("image/*", mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("application/x-docbook+xml", mime, EINA_FNMATCH_CASEFOLD);
}

static inline Eina_Bool
_entice_thumb_check_font(const char *mime)
{
  return eina_fnmatch("font/*", mime, EINA_FNMATCH_CASEFOLD);
}

static inline Eina_Bool
_entice_thumb_check_paged(const char *mime)
{
  return eina_fnmatch("application/*pdf", mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("application/acrobat", mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("application/postscript", mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("application/vnd.ms-powerpoint", mime,
                      EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("application/msword", mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("application/vnd.ms-word", mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch(
             "application/"
             "vnd.openxmlformats-officedocument.wordprocessingml.document",
             mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("application/vnd.oasis.opendocument.text*", mime,
                      EINA_FNMATCH_CASEFOLD);
}

static inline Eina_Bool
_entice_thumb_check_music(const char *mime)
{
  return eina_fnmatch("audio/mpeg", mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("audio/ogg", mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("audio/aac", mime, EINA_FNMATCH_CASEFOLD) ||
         eina_fnmatch("audio/flac", mime, EINA_FNMATCH_CASEFOLD);
}

static inline Eina_Bool
_entice_thumb_check_video(const char *mime)
{
    return eina_fnmatch("video/*", mime, EINA_FNMATCH_CASEFOLD);
}

static inline Eina_Bool
_entice_thumb_check_edje(const char *mime)
{
    return eina_fnmatch("application/x-edje", mime, EINA_FNMATCH_CASEFOLD);
}

static inline void
scale(int *w, int *h, int maxw, int maxh, Eina_Bool no_scale_up)
{ // write a big 1024x1024 preview (but no larger than the original image)
  int ww, hh;

  ww = maxw;
  hh = (*h * maxw) / *w;
  if (hh > maxh)
    { // too tall = so limit height and scale down keeping aspect
      hh = maxh;
      ww = (*w * maxh) / *h;
    }
  if ((no_scale_up) && ((ww > *h) || (hh > *h)))
    {
      ww = *w;
      hh = *h;
    }
  *w = ww;
  *h = hh;
}

static inline void
scale_out(int *w, int *h, int maxw, int maxh, Eina_Bool no_scale_up)
{ // write a big 1024x1024 preview (but no larger than the original image)
  int ww, hh;

  ww = maxw;
  hh = (*h * maxw) / *w;
  if (hh < maxh)
    { // not tall enough = so limit height and scale down keeping aspect
      hh = maxh;
      ww = (*w * maxh) / *h;
    }
  if ((no_scale_up) && ((ww > *h) || (hh > *h)))
    {
      ww = *w;
      hh = *h;
    }
  *w = ww;
  *h = hh;
}

#endif
