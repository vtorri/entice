/* Entice - small and simple image viewer using the EFL
 * Copyright (C) 2024 Vincent Torri
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <config.h>

#include <Elementary.h>

#include "entice_thumb.h"
#include "entice_thumb_gen.h"

Evas_Object *win    = NULL;
Evas_Object *subwin = NULL;
Evas_Object *image  = NULL;

static Eina_Bool
_entice_thumb_gen_dir_make(const char *path)
{
    char *dir;
    Eina_Bool res;

    dir = ecore_file_dir_get(path);
    if (!dir)
        return EINA_TRUE;

    res = ecore_file_mkpath(dir);
    free(dir);

    return res;
}

static Eet_File *
_entice_thumb_output_open(const char *thumb)
{
    // open target thumb tmp file we will later atomically replace target with
    char buf[PATH_MAX];

    snprintf(buf, sizeof(buf), "%s.tmp", thumb);
    return eet_open(buf, EET_FILE_MODE_WRITE);
}

static void
_entice_thumb_output_close(Eet_File *ef, const char *thumb)
{
    // close thumb file and atomically (not on Windows rename tmp file
    // on top of target
    char buf[PATH_MAX];
    Eina_Bool ret;

    eet_close(ef);
    snprintf(buf, sizeof(buf), "%s.tmp", thumb);
    // fix permissions on file to match parent dir
#ifndef _WIN32
    util_file_mode_parent_copy(buf, EINA_FALSE);
#endif
    ret = ecore_file_mv(buf, thumb);
    fflush(stdout);
}

EAPI_MAIN int
elm_main(int argc, char *argv[])
{
    unsigned char statsha1[20];
    Eet_File *ef;
    const char *path;  /* image file name */
    const char *mime;  /* image mime type */
    const char *thumb; /* output thumb file name (.eet) */
    int ret;

    if (argc < 4) return 100;

    path  = argv[1];
    mime  = argv[2];
    thumb = argv[3];

    // set up buffer win/canvas with sub win rendered inline as image
    elm_policy_set(ELM_POLICY_QUIT, ELM_POLICY_QUIT_LAST_WINDOW_CLOSED);
    elm_config_preferred_engine_set("buffer");
    win    = elm_win_add(NULL, "Entice-Thumb", ELM_WIN_BASIC);
    subwin = elm_win_add(win, "inlined", ELM_WIN_INLINED_IMAGE);
    elm_win_alpha_set(subwin, EINA_TRUE);
    image = elm_win_inlined_image_object_get(subwin);
    evas_object_show(subwin);
    evas_object_show(win);

    // manual rendering as we won't run the loop - only render then get results
    elm_win_norender_push(subwin);
    elm_win_norender_push(win);

    // stat orig file and store the state info we care about as a sha1 hash

    if (!entice_thumb_sha1(path, statsha1))
        exit(1);

    // ensure dest dir for thumb exists
    if (!_entice_thumb_gen_dir_make(thumb))
        exit(2);

    // open our thumb file to write things into it
    ef = _entice_thumb_output_open(thumb);
    if (!ef) exit(3);

    // an edj file (theme, background, icon .... groups inside matter
    if (_entice_thumb_check_edje(mime))
        ret = thumb_edje(ef, path, mime, thumb);
    // if it's a font - load it and render some text as thumb/preview
    else if (_entice_thumb_check_font(mime))
        ret = thumb_font(ef, path, mime, thumb);
    // a paged document - load multiple pages into thumb
    else if (_entice_thumb_check_paged(mime))
        ret = thumb_paged(ef, path, mime, thumb);
    // a music track/file
    else if (_entice_thumb_check_music(mime))
        ret = thumb_music(ef, path, mime, thumb);
    // a video file of a moive, series episode or something else
    else if (_entice_thumb_check_video(mime))
        ret = thumb_video(ef, path, mime, thumb);
    // otherwise handle as an image
    else ret = thumb_image(ef, path, mime, thumb);

    // write out the original file path so we could walk through all thumbs
    // and find which thumbs no longer have an original file left
    eet_write(ef, "orig/path", path, strlen(path), EET_COMPRESSION_LOW);
    // write out our sha1 of the file stat info - quick and mostly right
    // way to check if the thumb is up to date with file
    eet_write(ef, "orig/stat/sha1", statsha1, 20, EET_COMPRESSION_NONE);
    // done - finish file write and atomic rename
    _entice_thumb_output_close(ef, thumb);

    // if we failed to generate the thumb - delete what we were building
    if (ret != 0) unlink(thumb);

    return ret;
}
ELM_MAIN()
