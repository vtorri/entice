#ifndef _WIN32

#include <Eina.h>

#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

void
util_file_mode_parent_copy(const char *file, Eina_Bool is_dir)
{
  struct stat st;
  char *s, *dir_parent = strdup(file);

  if (!dir_parent) return;
  s = strrchr(dir_parent, '/');
  if (!s) goto err;
  s[1] = '\0'; // get parent dir by truncating after /
  if (lstat(dir_parent, &st) == 0)
    { // copy the parent dir mode to the child file given
      chown(file, st.st_uid, st.st_gid);
      // if it's not a dir - filter out execute mode
      if (!is_dir)
        st.st_mode &= S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
      chmod(file, st.st_mode);
    }
err:
  free(dir_parent);
}

#endif
